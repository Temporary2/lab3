from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
from articles.models import Article

def archive(request):
	return render(request, 'archive.html', {"posts": Article.objects.all()})